Jawaban 
Soal 1. Buat Data Base
mysql -uroot
create database myshop;
show databases;

Soal 2. Membuat Table di Dalam Database
create table user(
	-> id int primary key auto_increment,
	-> name varchar(255),
	-> email varchar(255),
	-> password varchar(255)
	-> );
describe user;

create table cayegories(
	-> id int primary key auto_increment,
	-> name varchar(255)
	-> user_id int,
	-> foreign key(user_id) references user(id)
	-> );
describe categories;

create table items(
	-> id int primary key auto_increment,
	-> name varchar(255),
	-> description varchar(255),
	-> price integer,
	-> stock integer,
	-> category_id int,
	-> foreign key(categories_id) references categories(id)
	-> );
describe items;

Soal 3. Memasukkan Data pada Table

#table User
insert into user(name, email, password) values ("john Doe", "John@doe.com","john123");
insert into user(name, email, password) values ("jane Doe", "Jane@doe.com","jenita123");
select * from user;

#table categories
insert into categories(name, user_id) values ("gadget",7);
insert into categories(name, user_id) values ("cloth",7);
insert into categories(name, user_id) values ("men",7);
insert into categories(name, user_id) values ("women",8);
insert into categories(name, user_id) values ("branded",8);
select * from categories;

#table items;
insert into items(name, description, price, stock, category_id) values ("Sumsang B50","Hape Keren merk sumsang","4000000","100",1);
insert into items(name, description, price, stock, category_id) values ("Uniklooh","Baju Keren dari Brand ternama","500000","50",2);
insert into items(name, description, price, stock, category_id) values ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10",1);
select * from items;

Soal 4. Mengambil Data dari Database
a. Mengambil data users
select id, name, email from user;

b. Mengambil data items;
select * from items where price > 1000000;
select * from items where name like '%Watch";

c. Menampilkan data items join dengan kategori
select items.id, items.name, items.description, items.price, items.stock, items.categories_id categories.name as kategory  from items inner join categories on item.categories_id = categories.id;

Soal 5. Mengubah Data dari Database
update items set price=2500000 where id=1;
select * from items;
